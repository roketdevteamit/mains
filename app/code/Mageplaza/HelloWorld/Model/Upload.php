<?php
/**
 * Mageplaza_HelloWorld extension
 *                     NOTICE OF LICENSE
 * 
 *                     This source file is subject to the Mageplaza License
 *                     that is bundled with this package in the file LICENSE.txt.
 *                     It is also available through the world-wide-web at this URL:
 *                     https://www.mageplaza.com/LICENSE.txt
 * 
 *                     @category  Mageplaza
 *                     @package   Mageplaza_HelloWorld
 *                     @copyright Copyright (c) 2016
 *                     @license   https://www.mageplaza.com/LICENSE.txt
 */
namespace Mageplaza\HelloWorld\Model;

class Upload
{
    /**
     * Upload model factory
     * 
     * @var \Magento\MediaStorage\Model\File\UploaderFactory
     * @var \Magento\Framework\Filesystem\Io\File
     */
    protected $_uploaderFactory;

    /**
     * constructor
     * 
     * @param \Magento\MediaStorage\Model\File\UploaderFactory $uploaderFactory
     * @param \Magento\Framework\Filesystem\Io\File $io
     */
    public function __construct(
        \Magento\MediaStorage\Model\File\UploaderFactory $uploaderFactory,
        \Magento\Framework\Filesystem\Io\File $io
    )
    {
        $this->_uploaderFactory = $uploaderFactory;
        $this->_io = $io;
    }

    /**
     * upload file
     *
     * @param $input
     * @param $destinationFolder
     * @param $data
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function uploadFileAndGetName($input, $destinationFolder, $data, $imgsF)
    {
        $zzz = $this->_io->rmdirRecursive($destinationFolder.'/'.$imgsF);
        $xxx = $this->_io->mkdir($destinationFolder.'/'.$imgsF, 0775);
        $count = count($_FILES[$input]['name']);

        for ($i=0; $i < $count; $i++) 
        {
            $target_dir = $destinationFolder.'/'.$imgsF.'/';
            $target_file = $target_dir . basename($_FILES[$input]["name"][$i]);
            $uploadOk = 1;
            $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

            // Check file size
            if ($_FILES[$input]["size"][$i] > 5000000) {
                $uploadOk = 0;
            }
            // Allow certain file formats
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" ) {
                $uploadOk = 0;
            }
            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
            // if everything is ok, try to upload file
            } else {
                if (move_uploaded_file($_FILES[$input]["tmp_name"][$i], $target_file)) {
                } else {
                    $uploadOk = 0;
                }
            }
        }

        return $destinationFolder.'/'.$imgsF;
    }
}
