<?php
/**
 * Mageplaza_HelloWorld extension
 *                     NOTICE OF LICENSE
 * 
 *                     This source file is subject to the Mageplaza License
 *                     that is bundled with this package in the file LICENSE.txt.
 *                     It is also available through the world-wide-web at this URL:
 *                     https://www.mageplaza.com/LICENSE.txt
 * 
 *                     @category  Mageplaza
 *                     @package   Mageplaza_HelloWorld
 *                     @copyright Copyright (c) 2016
 *                     @license   https://www.mageplaza.com/LICENSE.txt
 */
namespace Mageplaza\HelloWorld\Block\Adminhtml\Post\Edit\Tab;

class Post extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * Wysiwyg config
     * 
     * @var \Magento\Cms\Model\Wysiwyg\Config
     */
    protected $_wysiwygConfig;

    /**
     * Country options
     * 
     * @var \Magento\Config\Model\Config\Source\Locale\Country
     */
    protected $_countryOptions;

    /**
     * Country options
     * 
     * @var \Magento\Config\Model\Config\Source\Yesno
     */
    protected $_booleanOptions;

    /**
     * Sample Multiselect options
     * 
     * @var \Mageplaza\HelloWorld\Model\Post\Source\SampleMultiselect
     * @var \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    protected $_sampleMultiselectOptions;

    /**
     * constructor
     * 
     * @param \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig
     * @param \Magento\Config\Model\Config\Source\Locale\Country $countryOptions
     * @param \Magento\Config\Model\Config\Source\Yesno $booleanOptions
     * @param \Mageplaza\HelloWorld\Model\Post\Source\SampleMultiselect $sampleMultiselectOptions
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig,
        \Magento\Config\Model\Config\Source\Locale\Country $countryOptions,
        \Magento\Config\Model\Config\Source\Yesno $booleanOptions,
        \Mageplaza\HelloWorld\Model\Post\Source\SampleMultiselect $sampleMultiselectOptions,
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        array $data = []
    )
    {
        $this->_wysiwygConfig            = $wysiwygConfig;
        $this->_countryOptions           = $countryOptions;
        $this->_booleanOptions           = $booleanOptions;
        $this->_sampleMultiselectOptions = $sampleMultiselectOptions;
        parent::__construct($context, $registry, $formFactory, $data);
        $this->_products_select = $this->getProductCollection();
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        /** @var \Mageplaza\HelloWorld\Model\Post $post */
        $post = $this->_coreRegistry->registry('mageplaza_helloworld_post');
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('post_');
        $form->setFieldNameSuffix('post');
        $fieldset = $form->addFieldset(
            'base_fieldset',
            [
                'legend' => __('Post Information'),
                'class'  => 'fieldset-wide'
            ]
        );
        $fieldset->addType('image', 'Mageplaza\HelloWorld\Block\Adminhtml\Post\Helper\Image');
        $fieldset->addType('file', 'Mageplaza\HelloWorld\Block\Adminhtml\Post\Helper\File');
        if ($post->getId()) {
            $fieldset->addField(
                'post_id',
                'hidden',
                ['name' => 'post_id']
            );
        }
        $fieldset->addField(
            'tags',
            'radios',
            [
                'label' => __('Gallery Type'),
                'title' => __('Gallery Type'),
                'name' => 'tags',
                'required' => true,
                'values' => [
                                ['value'=>'1','label'=>'Upload'],
                                ['value'=>'2','label'=>'Script'],
                            ],
                'disabled' => true
            ]
        );
        $fieldset->addField(
            'status',
            'select',
            [
                'name'  => 'status',
                'label' => __('Produc'),
                'title' => __('Produc'),
                'required' => true,
                'options' => $this->_products_select
            ]
        );

        $fieldset->addField(
            'name',
            'text',
            [
                'name'  => 'name',
                'label' => __('Name'),
                'title' => __('Name'),
                'required' => true,
            ]
        );

        $fieldset->addField(
            'post_content',
            'textarea',
            [
                'name'  => 'post_content',
                'label' => __('Script Url'),
                'title' => __('Script Url'),
            ]
        )->setAfterElementHtml('
            <script>
                require([
                     "jquery",
                ], function($){
                    $(document).ready(function () {
                        if($("#post_tags1").attr("checked")=="checked"){
                            $("div.admin__field.field.field-post_content").fadeOut(0);
                            $( "#post_images" ).attr( "required", "true" );
                        }else{
                            $("div.admin__field.field.field-images").fadeOut(0);
                            $( "#post_post_content" ).attr( "required", "true" );
                        }
                                                
                        $( ".field-tags .admin__field-option .admin__field-label" ).click(function(){

                            if ($(this).attr("for")=="post_tags2") {
                                $("div.admin__field.field.field-images").fadeOut(0);
                                $("div.admin__field.field.field-post_content").fadeIn(0);
                                $( "#post_images" ).removeAttr( "required");
                                $( "#post_post_content" ).attr( "required", "true" );
                            }
                            else if ($(this).attr("for")=="post_tags1") 
                            {
                                $("div.admin__field.field.field-images").fadeIn(0);
                                $("div.admin__field.field.field-post_content").fadeOut(0);
                                $( "#post_post_content" ).removeAttr( "required");
                                $( "#post_images" ).attr( "required", "true" );
                            }
                        });
                    });
                  });
            </script>
        ');

        $fieldset->addField('images', 'image', array(
        'name'      => 'images[]',
        'multiple'  => true,
        'label'     => __('Images'),
        'title'     => __('Images'),
        'required'  => true
        ))->setAfterElementHtml('
            <script>
                require([
                     "jquery",
                ], function($){
                    $(document).ready(function () {
                        $( "#post_images" ).attr( "multiple", "true" );
                    });
                  });
           </script>
        ');
        

        $postData = $this->_session->getData('mageplaza_helloworld_post_data', true);
        if ($postData) {
            $post->addData($postData);
        } else {
            if (!$post->getId()) {
                $post->addData($post->getDefaultValues());
            }
        }
        $form->addValues($post->getData());
        $this->setForm($form);
        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return __('Post');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->getTabLabel();
    }

    /**
     * Can show tab in tabs
     *
     * @return boolean
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Tab is hidden
     *
     * @return boolean
     */
    public function isHidden()
    {
        return false;
    }

    public function getProductCollection() {
        $post = $this->_coreRegistry->registry('mageplaza_helloworld_post');

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('catalog_product_entity');
        
        $existGalleries = $connection->fetchAll("Select status FROM mageplaza_helloworld_post");
        
        //Select Data from table
        $sql = "Select * FROM " . $tableName;
        $products = $connection->fetchAll($sql);
        $result = [];

        if($post->getId()){
            $currentprod = $connection->fetchAll("Select name, post_id, status FROM mageplaza_helloworld_post WHERE post_id='".$post->getId()."'");
            $current_pr = $connection->fetchAll("Select * FROM catalog_product_entity WHERE entity_id=".$currentprod[0]['status']);
            $result[$currentprod[0]['status']] = '#'.$currentprod[0]['status'].' '.$current_pr[0]['sku'];
        }
        foreach ($products as $key => $product) {
            $isRepeat = false;
            foreach ($existGalleries as $key => $gallery) {
                if($gallery['status'] == $product['entity_id']){
                    $isRepeat = true;
                }
            }
            if(!$isRepeat){
                $result[$product['entity_id']] = '#'.$product['entity_id'].' '.$product['sku'];
            }     
        }
        return $result;
    }
}
